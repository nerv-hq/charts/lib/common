{{- define "common.podTemplate" -}}
{{/*
Construct list if volumes to be backedup
*/}}
{{- $backup := list }}
{{- if .Values.volumes.data.backup }}
{{- $backup = append $backup "data" }}
{{- end }}
{{- range $name, $val := .Values.volumes.extra }}
{{- if $val.backup }}
{{ $backup := append $backup $name }}
{{- end }}
{{- end }}
{{- range $name, $val := .Values.volumes.nfs }}
{{- if $val.backup }}
{{ $backup := append $backup $name }}
{{- end }}
{{- end }}
metadata:
  labels:
    {{- include "common.labels" . | nindent 4 }}
    {{- range $key, $val :=  .Values.podLabels }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- if (or .Values.podAnnotations $backup) }}
  annotations:
    backup.velero.io/backup-volumes: {{ join "," $backup }}
  {{- range $key, $val :=  .Values.podAnnotations }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
spec:
  {{- if .Values.hostNetwork }}
  hostNetwork: {{ .Values.hostNetwork | default false }}
  {{- end }}
  {{- if .Values.runtimeClassName }}
  runtimeClassName: {{ .Values.runtimeClassName }}
  {{- end }}
  {{- if .Values.SecurityContext }}
  securityContext: {{ toYaml .Values.SecurityContext | nindent 6 }}
  {{- end }}
  {{- if .Values.initContainers }}
  initContainers:
  {{- range $key, $val := .Values.initContainers }}
    {{- $val := merge $val (dict "Values" $.Values) }}
    {{- $val := merge $val (dict "Chart" $.Chart) }}
    {{- $val := merge $val (dict "Release" $.Release) }}
    {{- if not $val.name -}}
      {{- $_ := set $val "name" $key -}}
    {{- end -}}
    {{- include "common.containerTemplate" $val | nindent 4 }}
  {{- end }}
  {{- end }}
  containers:
  {{- range $key, $val := .Values.containers }}
    {{- $val := merge $val (dict "Values" $.Values) }}
    {{- $val := merge $val (dict "Chart" $.Chart) }}
    {{- $val := merge $val (dict "Release" $.Release) }}
    {{- if not $val.name -}}
      {{- $_ := set $val "name" $key -}}
    {{- end -}}
    {{- include "common.containerTemplate" $val | nindent 4 }}
  {{- end }}
  {{- if and .Values.volumes (coalesce .Values.volumes.extra .Values.volumes.nfs .Values.volumes.emptyDirs .Values.volumes.secrets .Values.volumes.configMap) }}
  volumes:
  {{- if .Values.volumes.extra }}
  {{- range $key, $val := .Values.volumes.extra }}
    - name: {{ $key }}
      persistentVolumeClaim:
        claimName: {{ include "common.fullname" $ }}-extra-{{ $key }}
  {{- end }}
  {{- end }}

  {{- if .Values.volumes.nfs }}
  {{- range $key, $val := .Values.volumes.nfs }}
    - name: {{ $key }}
      persistentVolumeClaim:
        claimName: {{ include "common.fullname" $ }}-nfs-{{ $key }}
  {{- end }}
  {{- end }}

  {{- if .Values.volumes.emptyDirs }}
  {{- range $key, $val := .Values.volumes.emptyDirs }}
    - name: {{ $key }}
      emptyDir:
        sizeLimit: {{ $val }}
  {{- end }}
  {{- end }}

  {{- if .Values.volumes.secrets }}
  {{- range $key, $val := .Values.volumes.secrets }}
    - name: {{ $key }}
      secret:
        secretName: {{ tpl $val.secretName $ }}
        {{- if $val.defaultMode }}
        defaultMode: {{ $val.defaultMode }}
        {{- end }}
        {{- if $val.items }}
        items: {{ $val.items | toYaml | nindent 10 }}
        {{- end }}
  {{- end }}
  {{- end }}

  {{- if .Values.volumes.configMap }}
  {{- range $key, $val := .Values.volumes.configMap }}
    - name: {{ $key }}
      configMap:
        name: {{ tpl $val.name $ }}
        {{- if $val.items }}
        items: {{ $val.items | toYaml | nindent 10 }}
        {{- end }}
  {{- end }}
  {{- end }}
  {{- end }}
{{- end }}
