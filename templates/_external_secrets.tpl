{{- define "common.externalSecrets" }}
{{- range $name, $val := .Values.externalSecrets }}
---
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: {{ include "common.fullname" $ }}-{{ $name }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
spec:
  refreshInterval: 1h
  secretStoreRef: {{ $val.secretStore | toYaml | nindent 4 }}
  target:
    name:
    {{- if $val.targetName }}
      {{ tpl $val.targetName $ | indent  1 }} # Name for the secret to be created on the cluster
    {{- else }}
      {{- include "common.fullname" $ | indent 1 }}-{{ $name }}
    {{- end }}
    creationPolicy: Owner
  data: {{ $val.data | toYaml | nindent 4 }}
{{- end }}
{{- end }}
