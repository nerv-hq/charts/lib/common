{{- define "common.containerTemplate" -}}

{{- if .enabled }}
  {{- $envMap := .Values.envMap }}
  {{- $envFromSecretKey := .Values.envFromSecretKey }}
  {{- $envFromConfigMapKey := .Values.envFromConfigMapKey }}
  {{- $envFromSecret := .Values.envFromSecret }}
  {{- $envFromConfigMap := .Values.envFromConfigMap -}}

  # Merge local and global envs
  {{- if .envMap -}}
  {{- $envMap := .envMap | mergeOverwrite $envMap }}
  {{- end -}}
  {{- if .envFromSecretKey -}}
  {{- $envFromSecretKey := .envFromSecretKey | mergeOverwrite $envFromSecretKey }}
  {{- end -}}
  {{- if .envFromConfigMapKey -}}
  {{- $envFromConfigMapKey := .envFromConfigMapKey | mergeOverwrite $envFromConfigMapKey }}
  {{- end -}}
  {{- if .envFromSecret -}}
  {{- $envFromSecret := .envFromSecret | concat $envFromSecret }}
  {{- end -}}
  {{- if .envFromConfigMap -}}
  {{- $envFromConfigMap := .envFromConfigMap | concat $envFromConfigMap }}
  {{- end }}

- name: {{ tpl (.name | quote) $ }}
  image: {{ .image.repository }}:{{ .image.tag | default "latest" }}
  {{- if .args }}
  args: {{ .args | toYaml | nindent 4 }}
  {{- end }}
  {{- if .command }}
  command: {{ .command | toYaml | nindent 4 }}
  {{- end }}
  {{- if .securityContext }}
  securityContext: {{- toYaml .securityContext | nindent 4 }}
  {{- end }}
  ports: {{ .ports | toYaml | nindent 4 }}
  {{- if .workingDir }}
  workingDir: {{ .workingDir }}
  {{- end }}
  env:
    # Regular env definition
    {{- if .env }}
    {{- tpl (.env | toYaml | nindent 4) $ }}
    {{- end }}
    # Env from map

    {{- range $key, $val := $envMap }}
    - name: {{ $key }}
      {{- if $val }}
      value: {{ tpl ($val | quote) $ }}
      {{- end -}}
    {{- end }}
    # Env from Secret Key
    {{- range $key, $val := $envFromSecretKey }}
    - name: {{ $key }}
      valueFrom:
        secretKeyRef:
          name: {{ tpl $val.secret $ }}
          key: {{ $val.key }}
    {{- end }}
    # Env from ConfigMap Key
    {{- range $key, $val := $envFromConfigMapKey }}
    - name: {{ $key }}
      valueFrom:
        configMapKeyRef:
          name: {{ tpl $val.configMap $ }}
          key: {{ $val.key }}
    {{- end }}

  {{- if .envFrom }}
  envFrom:
    # Regular envFrom definition
    {{- tpl (.envFrom | toYaml | nindent 4) $ }}
  {{- end }}
    # Env from Secret
    {{- range $key, $val := $envFromSecret }}
    - secretRef:
        name: {{ tpl $val $ }}
    {{- end }}
    # Env from ConfigMap
    {{- range $key, $val := $envFromConfigMap }}
    - configMapRef:
        name: {{ tpl $val $ }}
    {{- end }}
  {{- if .resources }}
  resources: {{ .resources | toYaml | nindent 4 }}
  {{- end }}
  {{- if .volumes }}
  volumeMounts:
  {{- tpl (.volumes | toYaml | nindent 4) $ }}
  {{- end }}
  {{- if .readiness }}
  readinessProbe: {{ .readiness | toYaml | nindent 4 }}
  {{- end }}
  {{- if .liveness }}
  livenessProbe: {{ .liveness | toYaml | nindent 4 }}
  {{- end }}
  {{- if .startup }}
  startupProbe: {{ .startup | toYaml | nindent 4 }}
  {{- end }}
{{- end }}
{{- end }}
