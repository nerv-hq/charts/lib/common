{{- define "common.daemonset" }}
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: {{ include "common.fullname" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
      matchLabels:
        app.kubernetes.io/name: {{ include "common.fullname" . }}
  template:
  {{- include "common.podTemplate" . | nindent 4 }}
{{- end }}
