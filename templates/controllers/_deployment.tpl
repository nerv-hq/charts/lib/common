{{- define "common.deployment" }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "common.fullname" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
      matchLabels:
        {{- include "common.labels" . | nindent 8 }}
  {{- if .Values.strategy }}
  strategy: {{ toYaml .Values.strategy | nindent 4 }}
  {{- end }}
  template:
  {{- include "common.podTemplate" . | nindent 4 }}
{{- end }}
