{{- define "common.statefulset" }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "common.fullname" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
      matchLabels:
        {{- include "common.labels" . | nindent 8 }}
  serviceName: {{ .Chart.Name }}
  template:
  {{- include "common.podTemplate" . | nindent 4 }}
  volumeClaimTemplates:
    - metadata:
        name: data
        namespace: {{ .Release.Namespace }}
        {{- if .Values.volumes.data.labels }}
        labels:
            {{- range $key, $val :=  .Values.volumes.data.labels }}
            {{ $key }}: {{ tpl $val $ }}
            {{ end -}}
        {{- end }}
        {{- if .Values.volumes.data.annotations -}}
        annotations:
          {{- range $key, $val := .Values.volumes.data.annotations }}
          {{ $key }}: {{ tpl $val $ }}
          {{- end }}
        {{- end }}
      spec:
        storageClassName: {{ .Values.volumes.data.storageClass | default $.Values.volumes.defaultStorageClass }}
        accessModes: {{ .Values.volumes.data.accessModes }}
        resources:
          requests:
            storage: {{ .Values.volumes.data.size }}
{{- end }}
