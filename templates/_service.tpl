{{- define "common.service" }}
{{- range $key, $service := .Values.services }}
{{- if $service.enabled }}
---
apiVersion: v1
kind: Service
metadata:
  name:  {{ include "common.fullname" $ }}-{{ $key }}
  namespace: {{ $.Release.Namespace }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
    {{- range $key, $label := $service.labels }}
      - {{ $key }}: {{ $label }}
    {{- end }}
  annotations: {{ $service.annotations | toYaml | nindent 4 }}
spec:
  type: {{ $service.type }}
  selector:
     app.kubernetes.io/name: {{ include "common.name" $ }}
  ports: {{ $service.ports | toYaml | nindent 4 }}
{{- end }}
{{- end }}
{{- end }}
