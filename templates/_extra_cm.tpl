{{- define "common.cm" }}
{{- range $key, $cm := .Values.extraConfigMap }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name:  {{ include "common.fullname" $ }}-{{ $key }}
  namespace: {{ $.Release.Namespace }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
    {{- range $key, $label := $cm.labels }}
      - {{ $key }}: {{ $label }}
    {{- end }}
  annotations: {{ $cm.annotations | toYaml | nindent 4 }}
data:
  {{- range $key, $val := $cm.data }}
    {{ $key }}: |-
      {{ $val | nindent 6 }}
  {{- end }}
{{- end }}
{{- end }}
