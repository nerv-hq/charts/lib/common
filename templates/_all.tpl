{{/*
Main entrypoint for the common library chart. It will render all underlying templates based on the provided values.
*/}}
{{- define "common.all" -}}
  {{- /* Merge the local chart values and the common chart defaults */ -}}
  {{- if .Values.common -}}
    {{- $defaultValues := deepCopy .Values.common -}}
    {{- $userValues := deepCopy (omit .Values "common") -}}
    {{- $mergedValues := mustMergeOverwrite $defaultValues $userValues -}}
    {{- $_ := set . "Values" (deepCopy $mergedValues) -}}
  {{- end -}}

  {{- if .Values.containers.main }}
    {{- if .Values.containers.main.enabled }}
      {{ if eq .Values.deployementType "StatefulSet" }}
        {{- include "common.statefulset" . | nindent 0  }}
      {{- end }}
      {{ if eq .Values.deployementType "Deployment" }}
        {{- include "common.deployment" . | nindent 0  }}
      {{- end }}
      {{ if eq .Values.deployementType "DaemonSet" }}
        {{- include "common.daemonset" . | nindent 0  }}
      {{- end }}
    {{- end -}}
  {{- end -}}

  {{- if .Values.volumes }}

  {{- if .Values.volumes.extra }}
  {{ include "common.persistance.extraPvc" . | nindent 0 }}
  {{- end -}}

  {{- if .Values.volumes.nfs }}
  {{ include "common.persistance.nfsPvc" . | nindent 0 }}
  {{ include "common.persistance.nfsPv" . | nindent 0 }}
  {{- end -}}

  {{- end -}}

  {{- if .Values.backup }}
  {{ include "common.backupSchedule" . | nindent 0 }}
  {{- end -}}

  {{ include "common.externalSecrets" . | nindent 0 }}

  {{ include "common.generatedSecrets" . | nindent 0 }}

  {{- if .Values.serviceMonitor }}
  {{ include "common.serviceMonitor" . | nindent 0 }}
  {{- end -}}

  {{ include "common.ingress" .  | nindent 0 }}

  {{ include "common.service" . | nindent 0 }}

  {{ include "common.cm" . | nindent 0 }}


{{- end -}}
