{{- define "common.ingress" }}
{{- range $key, $val := .Values.ingress }}
{{- if $val.enabled }}
---
apiVersion: traefik.io/v1alpha1
kind: IngressRoute
metadata:
  name: {{ include "common.fullname" $ }}-{{ $key }}
  namespace: {{ $.Release.Namespace }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
spec:
  entryPoints: {{ $val.entrypoints }}
  routes:
    - kind: Rule
      {{- if $val.rule }}
      match: {{ tpl $val.rule $ | quote }}
      {{- else }}
      match: Host(`{{ $val.host }}`)
      {{- end }}
      services:
        - kind: Service
          name: {{ tpl $val.service.name $ }}
          port: {{ $val.service.port }}
      middlewares:
      {{- if and $val.wss $val.wss.enabled }}
        - name: {{ include "common.fullname" $ }}-wss-{{ $key }}
      {{- end }}
    {{- if and $val.auth $val.auth.enabled }}
        - name: {{ include "common.fullname" $ }}-auth-{{ $key }}
    {{- if $val.auth.bypassAuth }}
    - kind: Rule
      match: Host(`{{ $val.host }}`)  && ( {{- range  $i, $v := $val.auth.bypassAuth -}} {{- if not (eq $i 0) -}} || {{- end -}}PathPrefix(`{{ $v }}`) {{- end -}} )
      services:
        - kind: Service
          name: {{ tpl $val.service.name $ }}
          port: {{ $val.service.port }}
    {{- end }}
    {{- end }}

{{- if and $val.tls $val.tls.enabled }}
  tls:
    secretName: {{ include "common.fullname" $ }}-tls-{{ $key }}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: {{ include "common.fullname" $ }}-tls-{{ $key }}
  namespace: {{ $.Release.Namespace }}
spec:
  dnsNames:
    - {{ $val.host }}
  secretName: {{ include "common.fullname" $ }}-tls-{{ $key }}
  issuerRef:
    name: {{ $val.tls.issuer | default "obito-fr-dns" }}
    kind: ClusterIssuer
{{- end }}

{{- if and $val.wss $val.wss.enabled }}
---
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: {{ include "common.fullname" $ }}-wss-{{ $key }}
  namespace: {{ $.Release.Namespace }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
spec:
  headers:
    customRequestHeaders:
      X-Forwarded-Proto: https
      # Connection: keep-alive, Upgrade
      # Upgrade: WebSocket
{{- end }}

{{- end }}
{{- end }}
{{- end }}
