{{- define "common.generatedSecrets" }}
{{- range $name, $val := .Values.generatedSecrets }}
---
apiVersion: secretgenerator.mittwald.de/v1alpha1
kind: StringSecret
metadata:
  name: {{ include "common.fullname" $ }}-{{ $name }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
spec:
  forceRegenerate: {{ $val.forceRegenerate | default "false" }}
  data: {{ $val.data | toYaml | nindent 4 }}
  fields: {{ $val.fields | toYaml | nindent 4 }}
{{- end }}
{{- end }}
