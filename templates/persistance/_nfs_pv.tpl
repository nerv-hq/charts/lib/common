{{- define "common.persistance.nfsPv" }}
{{- range $name,$val := .Values.volumes.nfs }}
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ include "common.fullname" $ }}-nfs-{{ $name }}
  namespace: {{ $.Release.Namespace }}
  {{- if $val.labels -}}
  labels:
    {{- range $key, $val :=  $val.labels }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
  {{- if .annotations -}}
  annotations:
    {{- range $key, $val :=  $val.annotations }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
spec:
  capacity:
    storage: 1Mi
  accessModes:
    - ReadWriteMany
  nfs:
    server: {{ $val.server }}
    path: {{ $val.path }}
  mountOptions:
    - nfsvers=4
{{- end }}
{{- end }}
