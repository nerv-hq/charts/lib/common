{{- define "common.persistance.nfsPvc" }}
{{- range $name,$val := .Values.volumes.nfs }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "common.fullname" $ }}-nfs-{{ $name }}
  namespace: {{ $.Release.Namespace }}
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: 1Mi
  volumeName: {{ include "common.fullname" $ }}-nfs-{{ $name }}
{{- end }}
{{- end }}
