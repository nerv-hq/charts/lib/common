{{- define "common.persistance.extraPvc" }}
{{- range $key, $val := .Values.volumes.extra }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "common.fullname" $ }}-extra-{{ $key }}
  namespace: {{ $.Release.Namespace }}
  annotations: {{ $val.annotations | toYaml | indent 4 }}
  {{- if $val.labels -}}
  labels:
    {{- range $key, $val :=  $val.labels }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
  {{- if $val.annotations -}}
  annotations:
    {{- range $key, $val :=  $val.annotations }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
spec:
  storageClassName: {{ $val.storageClass | default $.Values.volumes.defaultStorageClass }}
  accessModes: {{ $val.accessModes }}
  resources:
    requests:
      storage: {{ $val.size }}
{{- end }}
{{- end }}
