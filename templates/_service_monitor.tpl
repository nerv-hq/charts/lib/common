{{- define "common.serviceMonitor" }}
{{- if .Values.serviceMonitor.enabled }}
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ include "common.fullname" . }}
  namespace: {{ .Release.Namespace }}
  {{- if .Values.serviceMonitor.labels -}}
  labels:
    {{- include "common.labels" . | nindent 4 }}
    {{- range $key, $val :=  .Values.serviceMonitor.labels }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
  {{- if .Values.serviceMonitor.annotations -}}
  annotations:
    {{- range $key, $val :=  .Values.serviceMonitor.annotations }}
    {{ $key }}: {{ tpl $val $ }}
    {{ end -}}
  {{- end }}
spec:
  jobLabel: {{ tpl (.Values.serviceMonitor.job | default "") . }}
  targetLabels: {{ .Values.serviceMonitor.targetLabels | toYaml | nindent 4 }}
  podTargetLabels: {{ .Values.serviceMonitor.podTargetLabels | toYaml | nindent 4 }}
  selector:
    matchLabels:
      {{- range $key, $val := .Values.serviceMonitor.selectorLabels }}
      {{ $key }}: {{ tpl $val $ }}
      {{- end }}
  namespaceSelector:
    matchNames:
      - {{ .Release.Namespace }}
  endpoints:
    - port: {{ .Values.serviceMonitor.port | default "metrics" }}
      path: {{ .Values.serviceMonitor.path | default "/metrics" }}
      interval: {{ .Values.serviceMonitor.interval | default "15s" }}
      scrapeTimeout: {{ .Values.serviceMonitor.scrapeTimeout | default "10s" }}
      honorLabels: {{ .Values.serviceMonitor.honorLabels | default "false" }}
      relabelings: {{ .Values.serviceMonitor.relabelings | toYaml | nindent 8 }}
{{- end }}
{{- end }}
